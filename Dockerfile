FROM openjdk:8-jre-alpine

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JAVA_OPTS=""

RUN adduser -D -s /bin/sh app
WORKDIR /home/app

ADD entrypoint.sh entrypoint.sh
RUN chmod 755 entrypoint.sh && chown app:app entrypoint.sh
USER app

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 8000

ADD target/swagger-1.0.jar app.jar