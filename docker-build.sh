#!/bin/sh

docker rm -f busa-server
docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")
docker rmi -f $(docker images | grep "busa-server" | awk "{print \$3}")
docker build -t busa-server .
# Enable for tagging
docker tag busa-server anup1008/busa-server

