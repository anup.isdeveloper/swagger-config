package com.swagger.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AppController {

	@GetMapping("/app-name")
	public @ResponseBody String  getAppName() {
		return "Spring Swagger Demo";
	}
	
	@GetMapping("/get-app-key")
	public @ResponseBody String  getAppKey() {
		return "14D0A7761E5258928232985D5D045C1339536E55865F3563C76F138C65EC70B0";
	}
}
